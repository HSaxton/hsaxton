class Orders{

    constructor(){
        this.items = [];
    }

    /**
     * Setter method
     * @param  order 
     */
    addOrder(order){
        this.items.push(order);
    }

        /**
     * Remove method
     * @param  order 
     */
    removeOrder(order){
    var index = this.items.indexOf(item.name);
 
    if (index > -1) {
       arr.splice(index, 1);
        }
    }

    /**
     * Sort method
     * @param  order 
     */
    sortOrder(order){
        this.items.sort(order);
    }

    /**
     * Getter method
     */
    getList(){
        return this.items;
    }

    getTotal(){
        let total = 0;
        // Arrow function in EcmaScript6
        this.items.forEach((item)=>{
            total = total + item.price;
        });
        return total;
    }

}