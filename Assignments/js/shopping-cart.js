export class ShoppingCart{
    
    constructor(){
        this.orderTable = document.querySelector('.table');
        this.initialize();
    }

    initialize(){
        this.orderTable.innerHTML = `
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order 

Name</th>
                                <th scope="col">Order 

Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td> Table generated from 

class </td></tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">Total 

Price</td>
                                <td class="order-

total"></td>
                            </tr>
                        </tfoot>
                        `;
    }

    renderRows(rowList, total){
        let tableRow = '';
        for (let orderIndex = 0; orderIndex < 

rowList.length; orderIndex++) {
            const order = rowList[orderIndex];
            const orderName = order.name;
            const orderPrice = order.price;

            // Use back tick to create dynamic string
            // ${} interpolation to resolve javascript 

object into values
            tableRow = tableRow + `<tr>
                            <th scope="row">${orderIndex + 

1}</th>
                            <td>${orderName}</td>
                            <td>$${orderPrice}</td>
                        </tr>`;
        }

        const tableBodyElement = document.querySelector

('table > tbody');
        tableBodyElement.innerHTML = tableRow;
        document.querySelector('.order-total').innerHTML = 

total
    }
}