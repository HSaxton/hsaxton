import { Order } from './order';
import { ShoppingCart } from './shopping-cart';

const myOrders = new Orders();
const shoppingCart = new ShoppingCart();

function handleOrderForm(event, action) {
    event.preventDefault(); // prevent default will stop the default behaviour of form ( form reload is default behaviour)
    const orderNameElement = document.querySelector('#orderName');
    const orderPriceElement = document.querySelector('#orderPrice');
    const newOrder = new Order(orderNameElement.value, orderPriceElement.value);
    switch (action) {
        case "add":
            myOrders.addOrder(newOrder);
        break;

        case "remove":
            myOrders.removeOrder(newOrder);
        break;

        case "sort":
            myOrders.sortOrder();
        break;
    
        default:
            break;
    }
    const rowList = myOrders.getList();
    // tableBodyElement.innerHTML = generateDetailedOrderTable(shoppingCart.getList());
    const totalPrice = myOrders.getTotal();

    shoppingCart.renderRows(rowList, totalPrice);


    orderPriceElement.value = '';
    orderNameElement.value = '';
}

/*
function generateDetailedOrderTable(orderHistory) {
    let tableRow = '';
    for (let orderIndex = 0; orderIndex < orderHistory.length; orderIndex++) {
        const order = orderHistory[orderIndex];
        const orderName = order.name;
        const orderPrice = order.price;

        // Use back tick to create dynamic string
        // ${} interpolation to resolve javascript object into values
        tableRow = tableRow + `<tr>
                        <th scope="row">${orderIndex + 1}</th>
                        <td>${orderName}</td>
                        <td>$${orderPrice}</td>
                      </tr>`;
    }
    return tableRow;
} */